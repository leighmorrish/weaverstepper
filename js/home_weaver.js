(function () {
    // TODO: Organise code
    // TODO: Maybe show error logs for components that didn't start when clicked in the list
    // TODO: Reset timeout when app starts if isRestarting is true

    //this works - app.controller('componentController', ['$scope', '$http', '$q', '$interval', '$mdDialog', '$timeout', '$window', function ($scope, $http, $q, $interval, $mdDialog, $timeout, $window) {
    app.controller('componentController', ['$scope', '$http', '$q', '$interval', '$mdDialog', '$timeout', '$window', 'logAction', 'logNavigation', '$rootScope', function ($scope, $http, $q, $interval, $mdDialog, $timeout, $window, logAction, logNavigation, $rootScope) {
    

        $rootScope.username="Not Registered";
        $rootScope.version="0.0.0";

        $scope.startedComponents = 0;
        $scope.totalComponents = 0;
        $scope.currentResponse = null;
        $scope.isRestarting = null;
        $scope.componentsList = [];
        $scope.restartButtonColor = null;
        $scope.check_available_version = "";
        $scope.year = new Date().getFullYear();

        var launcher_url = 'https://demos.openet.com/';
        
        $scope.checkRegistration = function(){
            readTextFile("js/Registration.json", function(data){

                data = JSON.parse(data);
                console.log(data);

                if (data.user.firstName == ""){
                    showCreateDialog();
                } else {
                    $rootScope.username = data.user.firstName + " " + data.user.secondName;
                    console.log("Username : " + $rootScope.username);
                }
            });
        }

        $scope.checkVersion = function(){
            readTextFile("js/version.json", function(data){

                data = JSON.parse(data);
                console.log(data);

                if (data.version.number != ""){
                    $rootScope.version = data.version.number;
                }   
            });
        }

        $scope.backupLogs = function(){
            var fileName = "localLog.json";
            readTextFile("js/" + fileName, function(data){

                // Remove extra ',' at the end of the json
                console.log("Locallog [" + data.slice(0, -1) + "]");

                if (data.length > 1){
                    $http.post( logging_endpoint, data)
                        .then(function successCallback(response) {
                        console.log("Backed up logs successfully");

                        $http.post("js/WriteToJson.jsp?fileName="+fileName+"&payload=")
                            .then(function success(response) {
                            // Response from WriteToFile
                            console.log("Local logs cleaned");
                        });

                    }, function errorCallback(response) {
                        console.log("Failed to Log to remote server");
                    });
                }

            });
        }

        $scope.checkAvailableVersion = function(){
            readTextFile(available_version, function(data){

                data = JSON.parse(data);
                console.log(data);

                console.log("This version " + $rootScope.version + " : available version " +  data.version.number );
                if (data.version.number.replace('.', '') > $rootScope.version.replace('.', '')){
                    $scope.check_available_version = "New Version available : " + data.version.number;
                    displayToast("New Stepper VM available : " + data.version.number);
                }   
                else {
                    $scope.check_available_version = "You have the latest version";
                }
            });
        }


        function safelyParseJsonString(jsonString) {
            var parsed = undefined;
            try {
              parsed = JSON.parse(jsonString);
            } catch (e) {
              // Oh well, but whatever...
                console.log("cant parse this: "+ jsonString);
            }
            console.log("returning parsed:" + parsed);
            return parsed;
        }
        $scope.getWeaverHealth = function() {
            console.log("action");

            var frontend_url = 'http://10.0.104.46:28050';
            var api_prefix = '/api/v2/';
            var url = frontend_url + api_prefix +"health/status";

            var default_headers = {
                'Content-Type': 'application/json'
            };

            return $http.get(url, {headers: default_headers, data: ''}).then(function(response) {
                console.log("success: ");
                return $q.resolve(response.data.data.flow_result);
            }).catch(function(response) {
                /* HTTP status can be 5XX even if request was actually successful since FE returns status from HPOO */
                /* Check if service health statuses are hidden as a JSON string in the error response */
                if(response.data && response.data.data && response.data.data.error) {

                    console.log("do 1");
                    var flow_result = safelyParseJsonString(response.data.data.error);
                    if(flow_result && flow_result.Output && flow_result.Output.services) {
                        return $q.resolve(flow_result);
                    }
                }

                /* It must be a real error response if it gets this far */
                console.error("Failed to get system health status. Response : " + JSON.stringify(response));
                console.log("this is url "+ url, {headers: default_headers, data: ''});
                return $q.reject(response);
            });
        }

        $scope.openSystemHealthDialog = function(event) {
            logAction('{"platform":"Weaver", "version":"'+$rootScope.version+'", "username":"'+$rootScope.username+'","action":"Button Press : Health Check", "date":"'+new Date()+'"}');
            return $mdDialog.show({
                  templateUrl: 'systemHealthDialog.html',
                  targetEvent: event,
                  scope: $scope,
                  controller: ['$scope', function($scope) {
                      $scope.serverError = undefined;
                      $scope.services = undefined;

                      $scope.startedComponentsCounter = 0;
                      $scope.totalComponents = 0;
                      $scope.iconColor = null;
                      $scope.componentIcon = null;

                      $scope.getWeaverHealth().then(function(flow_result){
                          console.log("Retrieved system health status : "+ JSON.stringify(flow_result.Output.services));
                          var services = flow_result.Output.services;
                          services.sort(function(a, b){
                              return a.service_id.localeCompare(b.service_id);
                          });
                          $scope.totalComponents = services.length;
                          for (var i = 0; i < services.length; i++) {
                              var service = services[i];
                              for (var j = 0; j < service.instances.length; j++) {
                                  var instance = service.instances[j];
                                  instance.statusConfig = {};
                                  if(instance.status === 'running') {
                                      $scope.startedComponentsCounter ++;
                                      $scope.iconColor = "green";
                                      $scope.componentIcon = "done";

                                      instance.statusConfig.colourClass = 'up';
                                      instance.statusConfig.text = "Running";
                                  } else {
                                      $scope.iconColor = "red";
                                      $scope.componentIcon = "cloud_off";

                                      instance.statusConfig.colourClass = 'down';
                                      instance.statusConfig.text = "Not Running";
                                  }
                              }
                          }
                          $scope.services = services;

                      }).catch(function(response) {
                          console.error("Failed to get system health status : "+ JSON.stringify(response));
                          $scope.services = [];
                          $scope.serverError = response;
                      });

                      $scope.withoutUnderscores = function(input) {
                          return input.replace(/_/g, ' ');
                      };

                      $scope.dismiss = function() {
                          $mdDialog.hide();
                    };
                }]
            });
        };


         $scope.openSystemHealthList = function(event) {
            return $mdDialog.show({
                  templateUrl: 'systemHealthDialog.html',
                  targetEvent: event,
                  scope: $scope,
                  controller: ['$scope', function($scope) {
                      $scope.serverError = undefined;
                      $scope.services = undefined;

                      $scope.startedComponentsCounter = 0;
                      $scope.totalComponents = 0;
                      $scope.iconColor = null;
                      $scope.componentIcon = null;

                      $scope.getWeaverHealth().then(function(flow_result){
                          console.log("Retrieved system health status : "+ JSON.stringify(flow_result.Output.services));
                          var services = flow_result.Output.services;
                          services.sort(function(a, b){
                              return a.service_id.localeCompare(b.service_id);
                          });
                          $scope.totalComponents = services.length;
                          for (var i = 0; i < services.length; i++) {
                              var service = services[i];
                              for (var j = 0; j < service.instances.length; j++) {
                                  var instance = service.instances[j];
                                  instance.statusConfig = {};
                                  if(instance.status === 'running') {
                                      $scope.startedComponentsCounter ++;
                                      $scope.iconColor = "green";
                                      $scope.componentIcon = "done";

                                      instance.statusConfig.colourClass = 'up';
                                      instance.statusConfig.text = "Running";
                                  } else {
                                      $scope.iconColor = "red";
                                      $scope.componentIcon = "cloud_off";

                                      instance.statusConfig.colourClass = 'down';
                                      instance.statusConfig.text = "Not Running";
                                  }
                              }
                          }
                          $scope.services = services;

                      }).catch(function(response) {
                          console.error("Failed to get system health status : "+ JSON.stringify(response));
                          $scope.services = [];
                          $scope.serverError = response;
                      });

                      $scope.withoutUnderscores = function(input) {
                          return input.replace(/_/g, ' ');
                      };

                      $scope.dismiss = function() {
                          $mdDialog.hide();
                    };
                }]
            });
        };

        $scope.navigate_launcher = function(){
            console.log("Check launcher availablity");
            canReachEnpoint(launcher_url);
        };

        function canReachEnpoint(url){
        // Simple GET request example:
        navigateToPage(url);
        };

        function navigateToPage(targetPage) {
            location.href = targetPage;
        }


    }]);

    app.controller('demoController', ['$scope', '$http', 'logAction', 'logNavigation', function ($scope, $http, logAction, logNavigation) {

        // Check if a card is disabled on the home page.
        $scope.isDisabled = function(isDisabled) {
            if (isDisabled) {
                return true;
            } else {
                return false;
            }
        };

        // Navigate to use case pages when a card is clicked.
        $scope.navigateToPage = function(card) {
            if (!$scope.isDisabled(card.isDisabled)) {
               $scope.logNavigation = logNavigation(card.link, card.link, "5559");
                //this works - location.href=card.link;
            }
        }

        $scope.weaverCards = [];

        // Populating array with Weaver use cases defined in a json file.
        $http.get('js/weaverUseCases.json').success(function(data){
            $scope.weaverCards = data;
        });
    }]);


}());
