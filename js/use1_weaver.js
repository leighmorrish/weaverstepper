app.controller('use1Controller', ['$scope', '$rootScope', '$mdDialog', '$q', '$http', '$timeout', 'getRequest', 'postRequest', 'createSubscriber', 'querySubscriber', 'displayRequestDialog', 'addOffer', 'displayToast', 'logAction', 'readTextFile', 'showCreateDialog', 'logNavigation', function($scope, $rootScope, $mdDialog, $q, $http, $timeout, getRequest, postRequest, createSubscriber, querySubscriber, displayRequestDialog, addOffer, displayToast, logAction, readTextFile, showCreateDialog, logNavigation){
//this works app.controller('use1Controller', ['$scope', '$rootScope', '$mdDialog', '$q', '$http', '$timeout', 'getRequest', 'postRequest', 'createSubscriber', 'querySubscriber', 'displayRequestDialog', 'addOffer', 'displayToast', function($scope, $rootScope, $mdDialog, $q, $http, $timeout, getRequest, postRequest, createSubscriber, querySubscriber, displayRequestDialog, addOffer, displayToast){
// ng-click='logNavigation("/offer-catalog/#/", "Offer-Catalog", appSelectorPort)' ??

    $scope.useCaseTitle = "Use Case 1";
    $scope.useCaseSubTitle = "Creating the Microblog VNF";

    
	$scope.checkRegistration = function(){
        readTextFile("js/Registration.json", function(data){
            data = JSON.parse(data);
            console.log(data);

            if (data.user.firstName == ""){
                showCreateDialog();
            } else {
                $rootScope.username = data.user.firstName + " " + data.user.secondName;
                console.log("Username : " + $rootScope.username);
            }
        });
    }
    $scope.checkRegistration();

    $scope.checkVersion = function(){
        readTextFile("js/version.json", function(data){
            data = JSON.parse(data);
            console.log(data);


            if (data.version.number != ""){
                $rootScope.version = data.version.number;
            }
        });
    }
    $scope.checkVersion();

    $scope.logNavigation = logNavigation;
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    
    $scope.toStep1 = function() {
        $timeout(function() {
            navigateToPage("#step-2");
        }, 100);
    }

    $scope.toStep2 = function() {
        $timeout(function() {
            navigateToPage("#step-3");
        }, 100);
    }

    $scope.toStep3 = function() {
        $timeout(function() {
            navigateToPage("#step-4");
        }, 100);
    }

    $scope.toStep4 = function() {
        $timeout(function() {
            navigateToPage("#step-5");
        }, 100);
    }

    $scope.toStep5 = function() {
        $timeout(function() {
            navigateToPage("#step-6");
        }, 100);
    }

    $scope.toStep6 = function() {
        $timeout(function() {
            navigateToPage("#step-7");
        }, 100);
    }

    $scope.toStep7 = function() {
        $timeout(function() {
            navigateToPage("#step-8");
        }, 100);
    }

    $scope.toStep8 = function() {
        $timeout(function() {
            navigateToPage("#step-9");
        }, 100);
    }

    $scope.toStep9 = function() {
        $timeout(function() {
            navigateToPage("#step-10");
        }, 100);
    }

    $scope.toStep10 = function() {
        $timeout(function() {
            navigateToPage("#step-11");
        }, 100);
    }

    $scope.someFunction = function() {
        console.log("YAY");
    }
    
    var dialog = document.querySelector('dialog');

    if (! dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }
    
    dialog.querySelector('.close').addEventListener('click', function() {
        dialog.close();
    });
    
    
    $scope.tvnf = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VNF Workspace";
        dialog.querySelector('#dialogText').innerHTML = "The VNF workspace is a centralised space for organising all the components and resources for the VNF. The VNF Template is a version controlled snapshot of the workspace.";       
    }
    
    $scope.tvnfc = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VNFC";
        dialog.querySelector('#dialogText').innerHTML = "A VNFC is a Component of a VNF - one or more work together to make up the VNF.";       
    }
    
    $scope.tvnft = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VNF Template";
        dialog.querySelector('#dialogText').innerHTML = "The VNF Template is a version controlled snapshot of the workspace - the template is called along with a VNFD to instantiate the VNF.";       
    }
    
    $scope.tvnfd = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VNFD";
        dialog.querySelector('#dialogText').innerHTML = "The VNF Descriptor is used by the VIM - in this case OpenStack - to specify resources for the VNF.";       
    }
    
    $scope.tvnfi = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Instantation";
        dialog.querySelector('#dialogText').innerHTML = "The user triggers an instantiatation flow, specifying the VNF Template and VNFD.";       
    }
    
    $scope.tvnff = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VNF Instance";
        dialog.querySelector('#dialogText').innerHTML = "The VNF is now instantiated and running. The user can run a Health Check workflow on the VNF to validate a successful installation.";       
    }
    
    $scope.conf = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "blogserver.conf.jinja";
        dialog.querySelector('#dialogText').innerHTML = "<code> dbconnect: user={{ metadata.user }} password={{ metadata.pass }} dbname={{ metadata.db }} host={{ topology.vnfc_ids.postgres.0.hostname }} port={{ metadata.dbport }}<br>listen_address: {{ metadata.listen_address }}<br>listen_port: {{ metadata.listen_port }}</code>";       
    }
    
    $scope.pack = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Packages";
        dialog.querySelector('#dialogText').innerHTML = "The Microblog VNF is comprised of 3 VNFCs, each with its own Packages and functionality. Weaver supports RPM and DEB packages (RedHat Package Manager/Debian Packages, are software packages for Linux/Ubuntu/Debian systems) <br><br>Blogserver - microblog-1.0.0-1el7.centos.x86_64.rpm<br>Nginx - nginx-1.8.1-1.el7.ngx.x86_64.rpm<br>Postgres - fetched from yum repositories";       
    }
        
    $scope.proc = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Procedures";
        dialog.querySelector('#dialogText').innerHTML = "<span style=\"color: #888888\">#!/usr/bin/python</span>"
+"<br><span style=\"color: #888888\"># ===============================================================</span>"
+"<br><span style=\"color: #888888\">#</span>"
+"<br><span style=\"color: #888888\">#  Copyright (c) Openet, 2016</span>"
+"<br><span style=\"color: #888888\">#</span>"
+"<br><span style=\"color: #888888\">#  This material is proprietary to Openet.</span>"
+"<br><span style=\"color: #888888\">#  All rights reserved</span>"
+"<br><span style=\"color: #888888\">#</span>"
+"<br><span style=\"color: #888888\"># ===============================================================</span>"
+"<br>"
+"<br><span style=\"color: #DD4422\">&quot;&quot;&quot;</span>"
+"<br><span style=\"color: #DD4422\">This script read yaml file passed through command line, looking for</span>"
+"<br><span style=\"color: #DD4422\">&Tab;.rpm packages need to be installed and install them</span>"
+"<br><span style=\"color: #DD4422\">&quot;&quot;&quot;</span>"
+"<br><span style=\"color: #008800; font-weight: bold\">import</span> <span style=\"color: #0e84b5; font-weight: bold\">os</span>"
+"<br><span style=\"color: #008800; font-weight: bold\">import</span> <span style=\"color: #0e84b5; font-weight: bold\">sys</span>"
+"<br><span style=\"color: #008800; font-weight: bold\">import</span> <span style=\"color: #0e84b5; font-weight: bold\">logging</span>"
+"<br><span style=\"color: #008800; font-weight: bold\">import</span> <span style=\"color: #0e84b5; font-weight: bold\">os</span><span style=\"color: #333333\">,</span> <span style=\"color: #0e84b5; font-weight: bold\">getpass</span>"
+"<br>"
+"<br>"
+"<br><span style=\"color: #008800; font-weight: bold\">from</span> <span style=\"color: #0e84b5; font-weight: bold\">ovlm.parameters</span> <span style=\"color: #008800; font-weight: bold\">import</span> Params"
+"<br><span style=\"color: #008800; font-weight: bold\">from</span> <span style=\"color: #0e84b5; font-weight: bold\">ovlm</span> <span style=\"color: #008800; font-weight: bold\">import</span> logger"
+"<br><span style=\"color: #008800; font-weight: bold\">from</span> <span style=\"color: #0e84b5; font-weight: bold\">ovlm</span> <span style=\"color: #008800; font-weight: bold\">import</span> jinja_template"
+"<br><span style=\"color: #008800; font-weight: bold\">from</span> <span style=\"color: #0e84b5; font-weight: bold\">ovlm</span> <span style=\"color: #008800; font-weight: bold\">import</span> software_packages"
+"<br><span style=\"color: #008800; font-weight: bold\">from</span> <span style=\"color: #0e84b5; font-weight: bold\">ovlm</span> <span style=\"color: #008800; font-weight: bold\">import</span> systemd_services"
+"<br>"
+"<br><span style=\"color: #008800; font-weight: bold\">if</span> __name__ <span style=\"color: #333333\">==</span> <span style=\"background-color: #fff0f0\">&quot;__main__&quot;</span>:"
+"<br>&Tab;<span style=\"color: #008800; font-weight: bold\">if</span> <span style=\"color: #007020\">len</span>(sys<span style=\"color: #333333\">.</span>argv) <span style=\"color: #333333\">&lt;</span> <span style=\"color: #0000DD; font-weight: bold\">2</span>:"
+"<br>&Tab;&Tab;sys<span style=\"color: #333333\">.</span>exit(<span style=\"background-color: #fff0f0\">&#39;Usage: &#39;</span> <span style=\"color: #333333\">+</span> __file__ <span style=\"color: #333333\">+</span> <span style=\"background-color: #fff0f0\">&#39; YAML_CONFIG_PATH&#39;</span>)"
+"<br>&Tab;PARAMS <span style=\"color: #333333\">=</span> Params(sys<span style=\"color: #333333\">.</span>argv[<span style=\"color: #0000DD; font-weight: bold\">1</span>])"
+"<br>"
+"<br>&Tab;logging_path <span style=\"color: #333333\">=</span> PARAMS<span style=\"color: #333333\">.</span>get_param(<span style=\"background-color: #fff0f0\">&#39;logging_path&#39;</span>)"
+"<br>&Tab;logging_level <span style=\"color: #333333\">=</span> PARAMS<span style=\"color: #333333\">.</span>get_param(<span style=\"background-color: #fff0f0\">&#39;logging_level&#39;</span>)"
+"<br>"
+"<br>&Tab;logger<span style=\"color: #333333\">.</span>initialize(logging_path, logging_level)"
+"<br>&Tab;logger<span style=\"color: #333333\">.</span>write(PARAMS<span style=\"color: #333333\">.</span>get_dump())"
+"<br>"
+"<br>&Tab;<span style=\"color: #888888\">#logging.info(&#39;Environment thinks the user is &quot;%s&quot;&#39;,os.getlogin());</span>"
+"<br>&Tab;<span style=\"color: #888888\">#logging.info(&#39;Effective user is &quot;%s&quot;&#39;,getpass.getuser());</span>"
+"<br>"
+"<br>&Tab;ALLPARAMS <span style=\"color: #333333\">=</span> PARAMS<span style=\"color: #333333\">.</span>get_all_params()"
+"<br>&Tab;workflow_type <span style=\"color: #333333\">=</span> ALLPARAMS[<span style=\"background-color: #fff0f0\">&#39;request&#39;</span>]<span style=\"color: #333333\">.</span>get(<span style=\"background-color: #fff0f0\">&#39;workflow_type&#39;</span>)"
+"<br>&Tab;logging<span style=\"color: #333333\">.</span>info(<span style=\"background-color: #fff0f0\">&#39;Action triggered for workflow_type: &quot;</span><span style=\"background-color: #eeeeee\">%s</span><span style=\"background-color: #fff0f0\">&quot;&#39;</span>, workflow_type)"
+"<br>"
+"<br>&Tab;PACKAGES_TO_INSTALL <span style=\"color: #333333\">=</span> PARAMS<span style=\"color: #333333\">.</span>get_param(<span style=\"background-color: #fff0f0\">&#39;packages&#39;</span>)"
+"<br>&Tab;VNFC_PATH <span style=\"color: #333333\">=</span> jinja_template<span style=\"color: #333333\">.</span>get_vnfc_path(PARAMS)"
+"<br>"
+"<br>&Tab;logging<span style=\"color: #333333\">.</span>info(<span style=\"background-color: #fff0f0\">&#39;Next packages will be installed: &#39;</span> <span style=\"color: #333333\">+</span> <span style=\"background-color: #fff0f0\">&#39;, &#39;</span><span style=\"color: #333333\">.</span>join(PACKAGES_TO_INSTALL))"
+"<br>&Tab;<span style=\"color: #008800; font-weight: bold\">for</span> pkg <span style=\"color: #000000; font-weight: bold\">in</span> PACKAGES_TO_INSTALL:"
+"<br>&Tab;&Tab;pkg_path <span style=\"color: #333333\">=</span> os<span style=\"color: #333333\">.</span>path<span style=\"color: #333333\">.</span>join(VNFC_PATH, pkg)"
+"<br>&Tab;&Tab;<span style=\"color: #008800; font-weight: bold\">if</span> software_packages<span style=\"color: #333333\">.</span>get_linux_distribution() <span style=\"color: #333333\">==</span> <span style=\"background-color: #fff0f0\">&#39;Ubuntu&#39;</span>:"
+"<br>&Tab;&Tab;&Tab;<span style=\"color: #008800; font-weight: bold\">if</span> <span style=\"color: #000000; font-weight: bold\">not</span> software_packages<span style=\"color: #333333\">.</span>is_deb_installed(pkg_path):"
+"<br>&Tab;&Tab;&Tab;&Tab;software_packages<span style=\"color: #333333\">.</span>install_deb(pkg_path)"
+"<br>&Tab;&Tab;&Tab;<span style=\"color: #008800; font-weight: bold\">else</span>:"
+"<br>&Tab;&Tab;&Tab;&Tab;logging<span style=\"color: #333333\">.</span>info(<span style=\"background-color: #fff0f0\">&#39;Package </span><span style=\"background-color: #eeeeee\">%s</span><span style=\"background-color: #fff0f0\"> is already installed&#39;</span>, pkg)"
+"<br>&Tab;&Tab;<span style=\"color: #008800; font-weight: bold\">else</span>:"
+"<br>&Tab;&Tab;&Tab;pakg <span style=\"color: #333333\">=</span> os<span style=\"color: #333333\">.</span>path<span style=\"color: #333333\">.</span>basename(pkg_path)"
+"<br>&Tab;&Tab;&Tab;pkg_name <span style=\"color: #333333\">=</span> os<span style=\"color: #333333\">.</span>path<span style=\"color: #333333\">.</span>splitext(pakg)[<span style=\"color: #0000DD; font-weight: bold\">0</span>]"
+"<br>&Tab;&Tab;&Tab;<span style=\"color: #008800; font-weight: bold\">if</span> <span style=\"color: #000000; font-weight: bold\">not</span> software_packages<span style=\"color: #333333\">.</span>is_rpm_installed(pkg_name):"
+"<br>&Tab;&Tab;&Tab;&Tab;software_packages<span style=\"color: #333333\">.</span>install_rpm(pkg_path)"
+"<br>&Tab;&Tab;&Tab;<span style=\"color: #008800; font-weight: bold\">else</span>:"
+"<br>&Tab;&Tab;&Tab;&Tab;logging<span style=\"color: #333333\">.</span>info(<span style=\"background-color: #fff0f0\">&#39;Package </span><span style=\"background-color: #eeeeee\">%s</span><span style=\"background-color: #fff0f0\"> is already installed&#39;</span>, pkg)"
+"<br>"
+"<br>&Tab;<span style=\"color: #008800; font-weight: bold\">print</span> <span style=\"background-color: #fff0f0\">&#39;Package(s): &#39;</span> <span style=\"color: #333333\">+</span> <span style=\"background-color: #fff0f0\">&#39;, &#39;</span><span style=\"color: #333333\">.</span>join(PACKAGES_TO_INSTALL) <span style=\"color: #333333\">+</span> <span style=\"background-color: #fff0f0\">&#39; were installed successfully&#39;</span>";       
    }
            
    $scope.meta = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "meta.yml";
        dialog.querySelector('#dialogText').innerHTML = "<span style=\"color: #0066BB; font-weight: bold\">dynamic_config_vnfc:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">60</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">configurations:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">configuration</span><span style=\"color: #996633\">/blogserver.conf.jinja</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">configuration</span><span style=\"color: #996633\">/blogserver.service.jinja</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">user:</span> <span style=\"color: #0066BB; font-weight: bold\">microblog</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">pass:</span> <span style=\"color: #0066BB; font-weight: bold\">supersecret</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">db:</span> <span style=\"color: #0066BB; font-weight: bold\">microblog</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">dbport:</span> <span style=\"color: #6600EE; font-weight: bold\">5432</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">listen_address:</span> <span style=\"color: #0066BB; font-weight: bold\">0.0.0.0</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">listen_port:</span> <span style=\"color: #6600EE; font-weight: bold\">80</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">dynamic_config_vnfc_v2.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">install_vnfc:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">120</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">packages:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">packages</span><span style=\"color: #996633\">/microblog-1.0.0-1.el7.centos.x86_64.rpm</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">install_vnfc_v2.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">uninstall_vnfc:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">120</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">configurations:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #996633\">/etc/blogserver/blogserver.conf</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #996633\">/usr/lib/systemd/system/blogserver.service</span>                      "
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">packages:</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">#</span> <span style=\"color: #0066BB; font-weight: bold\">BEFORE_UPGRADE_BEGIN</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">packages</span><span style=\"color: #996633\">/microblog-1.0.0-1.el7.centos.x86_64.rpm</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">#</span> <span style=\"color: #0066BB; font-weight: bold\">BEFORE_UPGRADE_END</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">#</span> <span style=\"color: #0066BB; font-weight: bold\">AFTER_UPGRADE_BEGIN</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">#</span>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">packages</span><span style=\"color: #996633\">/microblog-1.0.0-99.el7.centos.x86_64.rpm</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">#</span> <span style=\"color: #0066BB; font-weight: bold\">AFTER_UPGRADE_END</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">uninstall_vnfc_v2.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">is_vnfc_down:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">10</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">services:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">blogserver</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">is_vnfc_down_v2.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">is_vnfc_up:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">10</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">services:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">blogserver</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">is_vnfc_up_v2.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">start_vnfc:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">10</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">services:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">blogserver</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">start_vnfc_v2.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">stop_vnfc:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">10</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">services:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">blogserver</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">stop_vnfc_v2.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">update_config_vnfc:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">destFile:</span> <span style=\"color: #996633\">/etc/blogserver/blogserver.conf</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">group:</span> <span style=\"color: #0066BB; font-weight: bold\">ovlm</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">permission:</span> <span style=\"color: #0066BB; font-weight: bold\">&#39;0644&#39;</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">srcFile:</span> <span style=\"color: #0066BB; font-weight: bold\">configuration</span><span style=\"color: #996633\">/blogserver.conf</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">user:</span> <span style=\"color: #0066BB; font-weight: bold\">ovlm</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">destFile:</span> <span style=\"color: #996633\">/usr/lib/systemd/system/blogserver.service</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">group:</span> <span style=\"color: #0066BB; font-weight: bold\">ovlm</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">permission:</span> <span style=\"color: #0066BB; font-weight: bold\">&#39;0644&#39;</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">srcFile:</span> <span style=\"color: #0066BB; font-weight: bold\">configuration</span><span style=\"color: #996633\">/blogserver.service</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">user:</span> <span style=\"color: #0066BB; font-weight: bold\">ovlm</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">update_config_vnfc_v3.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">heal_vnfc:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">60</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">services:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">blogserver</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">heal_vnfc_v2.py</span>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">upgrade_vnfc:</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">timeout:</span> <span style=\"color: #6600EE; font-weight: bold\">60</span>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">parameters:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">services:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">blogserver</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">uninstall_packages:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">packages</span><span style=\"color: #996633\">/microblog-1.0.0-1.el7.centos.x86_64.rpm</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">install_packages:</span>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">packages</span><span style=\"color: #996633\">/microblog-1.0.0-99.el7.centos.x86_64.rpm</span>"
+"<br><br>  <span style=\"color: #0066BB; font-weight: bold\">procedure_name:</span> <span style=\"color: #0066BB; font-weight: bold\">upgrade_vnfc_v2.py</span>";       
    }

     $scope.vnfdfile = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "vnfd.yml";
        dialog.querySelector('#dialogText').innerHTML = "<span style=\"color: #0066BB; font-weight: bold\">tosca_definitions_version:</span> <span style=\"color: #0066BB; font-weight: bold\">openet_tosca_nfv_1_0_0</span>"
+"<br> "
+"<br><span style=\"color: #0066BB; font-weight: bold\">description:</span> <span style=\"color: #0066BB; font-weight: bold\">Example</span> <span style=\"color: #0066BB; font-weight: bold\">Weaver</span> <span style=\"color: #0066BB; font-weight: bold\">Microblog</span> <span style=\"color: #0066BB; font-weight: bold\">VNFD</span>"
+"<br>"
+"<br><span style=\"color: #0066BB; font-weight: bold\">topology_template:</span>"
+"<br>"
+"<br>  <span style=\"color: #0066BB; font-weight: bold\">node_templates:</span>"
+"<br>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">node-3:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.VDU</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">vnfc_id:</span> <span style=\"color: #0066BB; font-weight: bold\">blogserver</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">availability_zone:</span> <span style=\"color: #0066BB; font-weight: bold\">nova</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">capabilities:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">host:</span>"
+"<br>         <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">num_cpus:</span> <span style=\"color: #6600EE; font-weight: bold\">1</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">disk_size:</span> <span style=\"color: #6600EE; font-weight: bold\">40</span> <span style=\"color: #0066BB; font-weight: bold\">GB</span>                         "
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">mem_size:</span> <span style=\"color: #6600EE; font-weight: bold\">2048</span> <span style=\"color: #0066BB; font-weight: bold\">MB</span>         "
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">os:</span>"
+"<br>          <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">architecture:</span> <span style=\"color: #0066BB; font-weight: bold\">x86_64</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">Linux</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">distribution:</span> <span style=\"color: #0066BB; font-weight: bold\">CentOS</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">version:</span> <span style=\"color: #0066BB; font-weight: bold\">7.0.0</span>"
+"<br>       "
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">CP3:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.CP</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">use_dynamic_floating_ip:</span> <span style=\"color: #0066BB; font-weight: bold\">True</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">source_network:</span> <span style=\"color: #0066BB; font-weight: bold\">admin_floating_net</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">security_groups:</span>"
+"<br>          <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">default</span>"
+"<br>          <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">test-sec-group</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">requirements:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">virtualLink:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">node:</span> <span style=\"color: #0066BB; font-weight: bold\">VL1</span>                   "
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">relationship:</span> <span style=\"color: #0066BB; font-weight: bold\">tosca.relationships.nfv.VirtualLinksTo</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">virtualBinding:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">node:</span> <span style=\"color: #0066BB; font-weight: bold\">node-3</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">relationship:</span> <span style=\"color: #0066BB; font-weight: bold\">tosca.relationships.nfv.VirtualBindsTo</span>"
+"<br>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">node-4:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.VDU</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">vnfc_id:</span> <span style=\"color: #0066BB; font-weight: bold\">blogserver</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">availability_zone:</span> <span style=\"color: #0066BB; font-weight: bold\">nova</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">capabilities:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">host:</span>"
+"<br>         <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">num_cpus:</span> <span style=\"color: #6600EE; font-weight: bold\">1</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">disk_size:</span> <span style=\"color: #6600EE; font-weight: bold\">40</span> <span style=\"color: #0066BB; font-weight: bold\">GB</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">mem_size:</span> <span style=\"color: #6600EE; font-weight: bold\">2048</span> <span style=\"color: #0066BB; font-weight: bold\">MB</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">os:</span>"
+"<br>          <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">architecture:</span> <span style=\"color: #0066BB; font-weight: bold\">x86_64</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">Linux</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">distribution:</span> <span style=\"color: #0066BB; font-weight: bold\">CentOS</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">version:</span> <span style=\"color: #0066BB; font-weight: bold\">7.0.0</span>"
+"<br>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">CP4:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.CP</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">use_dynamic_floating_ip:</span> <span style=\"color: #0066BB; font-weight: bold\">True</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">source_network:</span> <span style=\"color: #0066BB; font-weight: bold\">admin_floating_net</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">security_groups:</span>"
+"<br>          <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">default</span>"
+"<br>          <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">test-sec-group</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">requirements:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">virtualLink:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">node:</span> <span style=\"color: #0066BB; font-weight: bold\">VL1</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">relationship:</span> <span style=\"color: #0066BB; font-weight: bold\">tosca.relationships.nfv.VirtualLinksTo</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">virtualBinding:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">node:</span> <span style=\"color: #0066BB; font-weight: bold\">node-4</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">relationship:</span> <span style=\"color: #0066BB; font-weight: bold\">tosca.relationships.nfv.VirtualBindsTo</span>"
+"<br>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">node-2:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.VDU</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">vnfc_id:</span> <span style=\"color: #0066BB; font-weight: bold\">nginx</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">capabilities:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">host:</span>"
+"<br>         <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">num_cpus:</span> <span style=\"color: #6600EE; font-weight: bold\">1</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">disk_size:</span> <span style=\"color: #6600EE; font-weight: bold\">40</span> <span style=\"color: #0066BB; font-weight: bold\">GB</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">mem_size:</span> <span style=\"color: #6600EE; font-weight: bold\">2048</span> <span style=\"color: #0066BB; font-weight: bold\">MB</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">os:</span>"
+"<br>          <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">architecture:</span> <span style=\"color: #0066BB; font-weight: bold\">x86_64</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">Linux</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">distribution:</span> <span style=\"color: #0066BB; font-weight: bold\">CentOS</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">version:</span> <span style=\"color: #0066BB; font-weight: bold\">7.0.0</span>"
+"<br>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">CP2:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.CP</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">use_dynamic_floating_ip:</span> <span style=\"color: #0066BB; font-weight: bold\">True</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">source_network:</span> <span style=\"color: #0066BB; font-weight: bold\">admin_floating_net</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">requirements:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">virtualLink:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">node:</span> <span style=\"color: #0066BB; font-weight: bold\">VL1</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">relationship:</span> <span style=\"color: #0066BB; font-weight: bold\">tosca.relationships.nfv.VirtualLinksTo</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">virtualBinding:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">node:</span> <span style=\"color: #0066BB; font-weight: bold\">node-2</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">relationship:</span> <span style=\"color: #0066BB; font-weight: bold\">tosca.relationships.nfv.VirtualBindsTo</span>"
+"<br>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">node-5:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.VDU</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">vnfc_id:</span> <span style=\"color: #0066BB; font-weight: bold\">postgres</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">capabilities:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">host:</span>"
+"<br>         <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">num_cpus:</span> <span style=\"color: #6600EE; font-weight: bold\">1</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">disk_size:</span> <span style=\"color: #6600EE; font-weight: bold\">40</span> <span style=\"color: #0066BB; font-weight: bold\">GB</span>"
+"<br>           <span style=\"color: #0066BB; font-weight: bold\">mem_size:</span> <span style=\"color: #6600EE; font-weight: bold\">2048</span> <span style=\"color: #0066BB; font-weight: bold\">MB</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">os:</span>"
+"<br>          <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">architecture:</span> <span style=\"color: #0066BB; font-weight: bold\">x86_64</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">Linux</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">distribution:</span> <span style=\"color: #0066BB; font-weight: bold\">CentOS</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">version:</span> <span style=\"color: #0066BB; font-weight: bold\">7.0.0</span>"
+"<br>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">CP5:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.CP</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">use_dynamic_floating_ip:</span> <span style=\"color: #0066BB; font-weight: bold\">True</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">source_network:</span> <span style=\"color: #0066BB; font-weight: bold\">admin_floating_net</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">requirements:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">virtualLink:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">node:</span> <span style=\"color: #0066BB; font-weight: bold\">VL1</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">relationship:</span> <span style=\"color: #0066BB; font-weight: bold\">tosca.relationships.nfv.VirtualLinksTo</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">-</span> <span style=\"color: #0066BB; font-weight: bold\">virtualBinding:</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">node:</span> <span style=\"color: #0066BB; font-weight: bold\">node-5</span>"
+"<br>            <span style=\"color: #0066BB; font-weight: bold\">relationship:</span> <span style=\"color: #0066BB; font-weight: bold\">tosca.relationships.nfv.VirtualBindsTo</span>"
+"<br>"
+"<br>    <span style=\"color: #0066BB; font-weight: bold\">VL1:</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">type:</span> <span style=\"color: #0066BB; font-weight: bold\">openet.nodes.nfv.VL</span>"
+"<br>      <span style=\"color: #0066BB; font-weight: bold\">properties:</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">network_id:</span> <span style=\"color: #0066BB; font-weight: bold\">admin_internal_net</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">subnet_id:</span> <span style=\"color: #0066BB; font-weight: bold\">admin_internal_net__subnet</span>"
+"<br>        <span style=\"color: #0066BB; font-weight: bold\">vendor:</span> <span style=\"color: #0066BB; font-weight: bold\">openet</span>";
    }

}]);
