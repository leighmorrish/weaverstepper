(function(){
    var app = angular.module('DemoPlatformECSUse1', ['ngMaterial']);

    app.controller('globalController', ['$scope', '$timeout', function($scope, $timeout) {
        $scope.isResetting = false;
        $scope.pageUrl = url;

        $scope.resetDemo = function() {
            $scope.isResetting = true;
            $timeout(function() {
                resetUse();
                location.reload();
            }, 100);
        }

        $scope.create = function() {
            createSubscriber();
        }

        $scope.navigateToPage = function(targetPage) {
            location.href = targetPage;
        }
    }]);

    app.controller('use1Controller', ['$scope', function ($scope) {
        $scope.jumpTo = function(targetID) {
            location.href = targetID;
        }
    }]);

    app.controller('use1DialogController', ['$scope', function ($scope) {
        $scope.dialog = document.querySelector('#dialog');
        $scope.showConnectDialog = function() {
            $scope.dialog.showModal();
            $scope.dialog.querySelector('.mdl-dialog__title').innerHTML = "Open Connect App";
            $scope.dialog.querySelector('.ResponseText').innerHTML = "Login to the Connect App with the following credentials: <ul><li>Ph. Number: 353851664600</li><li>Password: 12345</li></ul> and navigate to the usage screen.";
        }
    }]);

    app.config(function ($mdThemingProvider, $mdToastProvider) {
		$mdThemingProvider.definePalette('openetDarkBluePalette', {
			'50': 'FFFFFF',
			'100': 'C8CFDE',
			'200': 'A4B0C8',
			'300': '7F90B2',
			'400': '6478A2',
			'500': '496192',
			'600': '3D558C',
			'700': '324988',
			'800': '2A4083',
			'900': '172D7C',
			'A100': '83A6EF',
			'A200': '56A0D5',
			'A400': '016A72',
			'A700': 'ee9537',
			'contrastDefaultColor': 'light',
			'contrastDarkColors': ['50', '100',
				'200', '300', 'A100'],
			'contrastLightColors': undefined
		});

		$mdThemingProvider.theme('default')
		.primaryPalette('openetDarkBluePalette', {
			'default': '500',
			'hue-1': '50',
			'hue-2': 'A400',
			'hue-3': 'A700'
		})
	});
}());
