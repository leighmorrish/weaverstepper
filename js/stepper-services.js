/* Function: Create subscriber in profile manager */
/* Params: Create subscriber request object in JSON format */
/* Returns: JSON object with - MSISDN, responseMessage, http response
   and a boolean stating if subscriber was created */
app.factory('createSubscriber', ['$q', 'postRequest', 'displayToast', function($q, postRequest, displayToast) {
    var createSubscriberInstance = function(request) {
        var MSISDN = angular.merge(request.INEAI_AddSubscriberRequest.MSISDN);
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var promise = postRequest("js/PostRestRequest.jsp?requestJSON=" + JSON.stringify(request) + "&url=" + provisioningUrl)
                promise.then(function successCallback(response) {
                    var responseMessage = "";
                    var created = false;
                    if(response.INEAI_AddSubscriberResponse.Response !== undefined) {
                        if(response.INEAI_AddSubscriberResponse.Response.Status === "SUCCESS") {
                            responseMessage = MSISDN + ": " + response.INEAI_AddSubscriberResponse.Response.Status;
                            created = true;
                        } else {
                            responseMessage = MSISDN + ": " + response.INEAI_AddSubscriberResponse.Response.ErrorResponse.ErrorMessage;
                        }
                        displayToast(responseMessage);
                    }
                    response = {
                        "MSISDN": MSISDN,
                        "responseMessage": responseMessage,
                        "response": response,
                        "created": created
                    }
                    resolve(response);
                }, function errorCallback(response) {
                    reject("Failed: " + JSON.stringify(response));
                });
            }, 1000);
        });
    }
    return createSubscriberInstance;
}]);

/* Function: Query a subscriber in profile manager */
/* Params: Query subscriber request object in JSON format */
/* Returns: JSON object with - MSISDN, http response
   and a boolean stating if subscriber exists */
app.factory('querySubscriber', ['$q', 'postRequest', function($q, postRequest) {
    var querySubscriberInstance = function(request) {
        var MSISDN = angular.merge(request.INEAI_QuerySubscriberRequest.SubscriberId);
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var promise = postRequest("js/PostRestRequest.jsp?requestJSON=" + JSON.stringify(request) + "&url=" + provisioningUrl)
                promise.then(function successCallback(response) {
                    var created = false;
                    if(response.INEAI_QuerySubscriberResponse !== undefined) {
                        if(response.INEAI_QuerySubscriberResponse.Response.Status == "SUCCESS") {
                            created = true;
                        }
                    }
                    response = {
                        "MSISDN": MSISDN,
                        "response": response,
                        "created": created
                    }
                    resolve(response);
                }, function errorCallback(response) {
                    reject("Failed: " + JSON.stringify(response));
                });
            }, 1000);
        });
    }
    return querySubscriberInstance;
}]);

/* Function: Add offer from OC to a subscribers profile */
/* Params: Add offer request in JSON format */
/* Returns: JSON object with - MSISDN, response message, http response
   and a boolean stating if offer was added */
app.factory('addOffer', ['$q', 'postRequest', 'displayToast', function($q, postRequest, displayToast) {
    var addOfferInstance = function(request) {
        var MSISDN = "";
        if(request.INEAI_AddOfferRequest.SubscriberId !== undefined) {
            MSISDN = angular.merge(request.INEAI_AddOfferRequest.SubscriberId);
        }
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var promise = postRequest("js/PostRestRequest.jsp?requestJSON=" + JSON.stringify(request) + "&url=" + provisioningUrl)
                promise.then(function successCallback(response) {
                    var responseMessage = "";
                    var added = false;
                    if(response.INEAI_AddOfferResponse !== undefined) {
                        if(response.INEAI_AddOfferResponse.Response.Status == "SUCCESS") {
                            if(MSISDN !== "") {
                                responseMessage = MSISDN + ": " + response.INEAI_AddOfferResponse.Response.Status;
                            } else {
                                responseMessage = response.INEAI_AddOfferResponse.Response.Status;
                            }
                            added = true;
                        } else {
                            if(MSISDN !== "") {
                                responseMessage = MSISDN + ": " + response.INEAI_AddOfferResponse.Response.ErrorResponse.ErrorMessage;
                            } else {
                                responseMessage = response.INEAI_AddOfferResponse.Response.ErrorResponse.ErrorMessage;
                            }
                        }
                        displayToast(responseMessage);
                    }
                    response = {
                        "MSISDN": MSISDN,
                        "responseMessage": responseMessage,
                        "response": response,
                        "added": added
                    }
                    resolve(response);
                }, function errorCallback(response) {
                    reject("Failed: " + JSON.stringify(response));
                });
            }, 1000);
        });
    }
    return addOfferInstance;
}]);

/* Function: Create empty subscriber group */
/* Params: Create group JSON request */
/* Returns: JSON object with - MSISDN, response message, http response
   and a boolean stating if offer was added */
app.factory('createGroup', ['$q', 'postRequest', 'displayToast', function($q, postRequest, displayToast) {
    var createGroupInstance = function(request) {
        var MSISDN = angular.merge(request.INEAI_CreateGroupRequest.MSISDN);
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var groupCreated = false;
                var responseMessage = "";
                var promise = postRequest("js/PostRestRequest.jsp?requestJSON=" + JSON.stringify(request) + "&url=" + provisioningUrl)
                promise.then(function successCallback(response) {
                    if(response.INEAI_CreateGroupResponse !== undefined) {
                        if(response.INEAI_CreateGroupResponse.Response.Status === "SUCCESS") {
                            groupCreated = true;
                            responseMessage = MSISDN + ": " + response.INEAI_CreateGroupResponse.Response.Status;
                        } else if(response.INEAI_CreateGroupResponse.Response.ErrorResponse.ErrorMessage !== undefined) {
                            responseMessage = MSISDN + ": " + response.INEAI_CreateGroupResponse.Response.ErrorResponse.ErrorMessage;
                        }
                        displayToast(responseMessage);
                    }
                    response = {
                        "MSISDN": MSISDN,
                        "responseMessage": responseMessage,
                        "response": response,
                        "groupCreated": groupCreated
                    }
                    resolve(response);
                }, function errorCallback(response) {
                    reject("Failed: " + JSON.stringify(response));
                });
            }, 1000);
        });
    }
    return createGroupInstance;
}]);

/* Function: Query subscriber group */
/* Params: Query group JSON request */
/* Returns: JSON object with - http response
   and a boolean stating if offer was added */
app.factory('queryGroup', ['$q', 'postRequest', 'displayToast', function($q, postRequest, displayToast) {
    var queryGroupInstance = function(request) {
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var groupCreated = false;
                var promise = postRequest("js/PostRestRequest.jsp?requestJSON=" + JSON.stringify(request) + "&url=" + provisioningUrl)
                promise.then(function successCallback(response) {
                    if(response.INEAI_QueryGroupResponse !== undefined) {
                        if(response.INEAI_QueryGroupResponse.Response.Status === "SUCCESS") {
                            groupCreated = true;
                        }
                    }
                    response = {
                        "response": response,
                        "groupCreated": groupCreated
                    }
                    resolve(response);
                }, function errorCallback(response) {
                    reject("Failed: " + JSON.stringify(response));
                });
            }, 1000);
        });
    }
    return queryGroupInstance;
}]);

app.factory('addSubsToGroup', ['$q', 'postRequest', 'displayToast', function($q, postRequest, displayToast) {
    var addSubsToGroupInstance = function(request) {
        var MSISDN = "";
        if(request.INEAI_AddSubscriberToGroupRequest !== undefined) {
            MSISDN = angular.merge(request.INEAI_AddSubscriberToGroupRequest.SubscriberId);
        }
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var subsInGroup = false;
                var responseMessage = "";
                var promise = postRequest("js/PostRestRequest.jsp?requestJSON=" + JSON.stringify(request) + "&url=" + provisioningUrl)
                promise.then(function successCallback(response) {
                    if(response.INEAI_AddSubscriberToGroupResponse !== undefined) {
                        if(response.INEAI_AddSubscriberToGroupResponse.Response.Status === "SUCCESS" && MSISDN !== "") {
                            subsInGroup = true;
                            responseMessage = MSISDN + ": " + response.INEAI_AddSubscriberToGroupResponse.Response.Status;
                        } else if(response.INEAI_AddSubscriberToGroupResponse.Response.ErrorResponse !== undefined && MSISDN !== "") {
                            responseMessage = MSISDN + ": " + response.INEAI_AddSubscriberToGroupResponse.Response.ErrorResponse.ErrorMessage;
                        } else {
                            responseMessage = "Error";
                        }
                    }
                    response = {
                        "MSISDN": MSISDN,
                        "response": response,
                        "responseMessage": responseMessage,
                        "groupCreated": subsInGroup
                    }
                    resolve(response);
                }, function errorCallback(response) {
                    reject("Failed: " + JSON.stringify(response));
                });
            }, 1000);
        });
    }
    return addSubsToGroupInstance;
}]);

app.factory('postRequest', ['$http', '$q', function($http, $q) {
    var postRequestInstance = function(resourceUrl) {
        return $q(function(resolve, reject) {
            setTimeout(function() {
                $http({
                    method: 'POST',
                    url: resourceUrl
                }).then(function successCallback(response) {
                    resolve(response.data);
                }, function errorCallback(response) {
                    reject("Post request failed: " + JSON.stringify(response));
                });
            }, 1000);
        });
    }
    return postRequestInstance;
}]);

app.factory('getRequest', ['$http', '$q', function($http, $q) {
    var getRequestInstance = function(resourceUrl) {
        return $q(function(resolve, reject) {
            setTimeout(function() {
                $http({
                    method: 'GET',
                    url: resourceUrl
                }).then(function successCallback(response) {
                    resolve(response.data);
                }, function errorCallback(response) {
                    reject("HTTP Error: Failure to retrieve: " + resourceUrl);
                });
            }, 1000);
        });
    }
    return getRequestInstance;
}]);

app.factory('displayRequestDialog', ['$mdDialog', '$timeout', function($mdDialog, $timeout){
    var displayRequestDialogInstance = function(ev, title, data) {
        $mdDialog.show({
            controller: requestDialogController,
            templateUrl: 'req-dialog.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
        })

        // Timeout to wait for dialog to open before injecting data
        $timeout(function() {
            document.querySelector('.dialog-title').innerHTML = title;
            document.querySelector('.md-dialog-content-body').innerHTML = Prism.highlight(JSON.stringify(data), Prism.languages.json);
            document.querySelector('.md-dialog-content-body').innerHTML = Prism.highlight(JSON.stringify(data), Prism.languages.json);
        }, 100);
    }

    function requestDialogController($scope) {
        $scope.hide = function() {
            $mdDialog.hide();
        };
    }
    return displayRequestDialogInstance;
}]);

app.factory('displayToast', ['$mdToast', function($mdToast) {
    var displayToastInstance = function(toastMessage) {
        $mdToast.show(
            $mdToast.simple()
            .textContent(toastMessage)
            .hideDelay(3000)
            .position('bottom left right')
        );
    }
    return displayToastInstance;
}]);

app.factory('logAction', ['$http', function($http) {
    var sendLoggingInfo = function(jsonPayload) {

        $http.post( logging_endpoint, jsonPayload)
            .then(function successCallback(response) {
            console.log("Logged "+jsonPayload+" successfully");
        }, function errorCallback(response) {
            console.log("Failed to Log to remote server");

            // Store local logs until access to the server is available again
            $http.post("js/AppendToJson.jsp?fileName=localLog.json&payload=" + encodeURIComponent(jsonPayload))
        });
    }
    return sendLoggingInfo;
}]);

app.factory('readTextFile', ['$http', function($http) {
    return function (file, callback) { 

        // Math.random is used to stop chrome from returning cached value
        $http.get(file + '?hash_id=' + Math.random(), {cache: false, transformResponse: undefined})
            .then(function(response) {
            callback(response.data);
        });
    }
}]);

app.factory('showCreateDialog', ['$mdDialog', '$http', 'logAction', '$rootScope', function($mdDialog, $http, logAction, $rootScope) {
    // Registration Dialog
    return function(ev) {
        $mdDialog.show({
            controller: RegisterationDialogController,
            templateUrl: 'register-dialog.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            escapeToClose:false
        })
    }

    // Funcitonality for the create Registration dialog
    function RegisterationDialogController($scope, $timeout) {

        $scope.user = {
            FirstName: '',
            LastName: ''
        };


        $scope.hide = function() {
            $mdDialog.hide();

            writeTextFile("Registration.json", $scope.user.FirstName, $scope.user.LastName, new Date());
        };
    }

    function writeTextFile(fileName, firstName, secondName, date) {
        $http.post("js/WriteToJson.jsp?fileName="+fileName+"&payload=" + encodeURIComponent("{\"user\":{\"firstName\":\"" + firstName + "\",\"secondName\":\"" + secondName + "\",\"date\":\"" + date + "\"}}"))


            .then(function success(response) {
            // Response from WriteToFile
            console.log("Registered [firstName="+firstName+"&secondName="+secondName+"&date="+date+"]");
            logAction('{"platform":"Weaver", "version":"'+$rootScope.version+'", "username":"'+firstName+' '+secondName+'","action":"Registered", "date":"'+date+'"}');
            $rootScope.username=firstName+' '+secondName;
        });
    }
}]);

app.factory('logNavigation', ['$rootScope', 'logAction', '$timeout', function($rootScope, logAction, $timeout){

    // Navigate to Openet Product
    return function(link, action, port) {
        console.log('"username":"'+$rootScope.username+'"');
        logAction('{"platform":"Weaver", "version":"'+$rootScope.version+'", "username":"'+$rootScope.username+'","action":"Navigation : '+action+'", "date":"'+new Date()+'"}');

        $timeout(function() {
            if(port === "60105") {
                window.open(url +":" + port + "/" + link, '_blank');
            } else {
                window.location.href = url +":" + port + "/" + link;
            }
            // window.location.target = '_blank';
            // window.location.href = url +":" + port + "/" + link;
        }, 600);
    }
}]);