// Some global variables
var url = "http://" + window.location.hostname;
var app = angular.module('DemoPlatform', ['ngMaterial']);
var provisioningUrl = ":1399/INEAI_Provisioning";
var igUrl = ":8181/ig/restapi";
var appSelectorPort = "60105";
var subscribers = [];
var currentSubscriber = "";
var requests = "";
var responses = new Map();
var currentRequest = "";
var currentResponse = "";
var subscriberExists = {"subID": "none", "exists": false};
var multipleSubscribers = false;
var currentSubscriberIndex = 0;
var logging_serverIP = 'http://34.207.106.118';
var available_version = logging_serverIP + ':5559/DemoPlatform/available_version.json';
var logging_endpoint = logging_serverIP + ':3000/log/';

// Navigate to the target page
function navigateToPage(targetPage) {
    location.href = targetPage;
}

// Check which subscribers exist
function checkSubscribers() {
    for(var i = 0; i < subscribers.length; i ++) {
        if(subscribers[i].isCreated) {
            subscriberExists.subID = subscribers[i].MSISDN;
            subscriberExists.exists = true;
            break;
        }
    }
}

function updateCreateRequest() {
    currentRequest.INEAI_AddSubscriberRequest.SubscriberId = currentSubscriber.SubscriberId;
    currentRequest.INEAI_AddSubscriberRequest.MSISDN = currentSubscriber.MSISDN;
    currentRequest.INEAI_AddSubscriberRequest.FirstName = currentSubscriber.FirstName;
    currentRequest.INEAI_AddSubscriberRequest.LastName = currentSubscriber.LastName;
    currentRequest.INEAI_AddSubscriberRequest.Type = currentSubscriber.Type;
}

(function () {
    window.addEventListener('load', function () {

        var selector ='.mdl-stepper';
        // Select stepper container element
        var stepperElement = document.querySelector(selector);
        var Stepper;
        var steps;

        if (!stepperElement) return;

        // Get the MaterialStepper instance of element to control it.
        Stepper = stepperElement.MaterialStepper;

        if (!Stepper) {
            console.error('MaterialStepper instance is not available for selector: ' + selector + '.');
            return;
        }
        steps = stepperElement.querySelectorAll('.mdl-step');

        for (var i = 0; i < steps.length; i++) {
            // When user clicks on [data-stepper-next] button of step.
            steps[i].addEventListener('onstepnext', function (e) {
                // {element}.MaterialStepper.next() change the state of current step to "completed"
                // and move one step forward.
                Stepper.next();
            });
            // When user clicks on [data-stepper-skip] button of step.
            steps[i].addEventListener('onstepskip', function (e) {
                Stepper.skip();
            });
        }

        // When all steps are completed this event is dispatched.
        stepperElement.addEventListener('onsteppercomplete', function (e) {
            var toast = document.querySelector('.mdl-js-snackbar');

            if (!toast){
                console.log("No Toast element available")
                return;
            }

            toast.MaterialSnackbar.showSnackbar({
                message: 'Completed Use Case',
                timeout: 4000,
                actionText: 'Ok'
            });

            //location.href='home.html';
        });
    })
})();

app.config(function ($mdThemingProvider, $mdToastProvider) {
    $mdThemingProvider.definePalette('openetDarkBluePalette', {
        '50': 'FFFFFF',
        '100': 'C8CFDE',
        '200': 'A4B0C8',
        '300': '7F90B2',
        '400': '6478A2',
        '500': '496192',
        '600': '3D558C',
        '700': '324988',
        '800': '2A4083',
        '900': '172D7C',
        'A100': '83A6EF',
        'A200': '56A0D5',
        'A400': '016A72',
        'A700': 'ee9537',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': ['50', '100',
            '200', '300', 'A100'],
        'contrastLightColors': undefined
    });

    $mdThemingProvider.theme('default')
    .primaryPalette('openetDarkBluePalette', {
        'default': '500',
        'hue-1': '50',
        'hue-2': 'A400',
        'hue-3': 'A700'
    })
});
