
app.controller('useintroController', ['$scope', '$rootScope', '$mdDialog', '$q', '$http', '$timeout', function($scope, $rootScope, $mdDialog, $q, $http, $timeout){
//app.controller('useintroController', ['$scope', '$rootScope', '$mdDialog', '$q', '$http', '$timeout', function($scope, $rootScope, $mdDialog, $q, $http, $timeout){
    $scope.useCaseTitle = "Introduction";
    $scope.useCaseSubTitle = "Introduction to Weaver";
    
    
	/*$scope.checkRegistration = function(){
        readTextFile("js/Registration.json", function(data){
            data = JSON.parse(data);
            console.log(data);

            if (data.user.firstName == ""){
                showCreateDialog();
            } else {
                $rootScope.username = data.user.firstName + " " + data.user.secondName;
                console.log("Username : " + $rootScope.username);
            }
        });
    }
    $scope.checkRegistration();

    $scope.checkVersion = function(){
        readTextFile("js/version.json", function(data){
            data = JSON.parse(data);
            console.log(data);


            if (data.version.number != ""){
                $rootScope.version = data.version.number;
            }
        });
    }
    $scope.checkVersion();

    $scope.logNavigation = logNavigation;
    
    */
    
    

    $scope.toStep1 = function() {
        $timeout(function() {
            navigateToPage("#step-2");
        }, 100);
    }

    $scope.toStep2 = function() {
        $timeout(function() {
            navigateToPage("#step-3");
        }, 100);
    }

    $scope.toStep3 = function() {
        $timeout(function() {
            navigateToPage("#step-4");
        }, 100);
    }

    $scope.toStep4 = function() {
        $timeout(function() {
            navigateToPage("#step-5");
        }, 100);
    }

    $scope.toStep5 = function() {
        $timeout(function() {
            navigateToPage("#step-6");
        }, 100);
    }

    $scope.toStep6 = function() {
        $timeout(function() {
            navigateToPage("#step-7");
        }, 100);
    }

    $scope.toStep7 = function() {
        $timeout(function() {
            navigateToPage("#step-8");
        }, 100);
    }

    $scope.toStep8 = function() {
        $timeout(function() {
            navigateToPage("#step-9");
        }, 100);
    }
    
    var dialog = document.querySelector('dialog');
    var dialogPolyfill = {};
    if (! dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }

    dialog.querySelector('.close').addEventListener('click', function() {
        dialog.close();
    });
    
    $scope.vnfm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VNF-M GUI";
        dialog.querySelector('#dialogText').innerHTML = "The Openet VNF-M GUI gives the user a single interface on which to perform VNF management tasks. The GUI consists of the VNF Catalog, VNF Lifecycle and the VNF Instances.";       
    }
    $scope.api = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "APIs an CLIs";
        dialog.querySelector('#dialogText').innerHTML = "Weaver provides REST APIs and CLI commands for working with the Workflow Engine, Configuration Manager, Performance Manager and the Fault Manager";
    }
    $scope.wfeg = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Workflow Engine GUI";
        dialog.querySelector('#dialogText').innerHTML = "Openets Workflow Engine GUI is a GUI interface for viewing workflows in greater detail. The GUI can be found on port 8089.";
    }
    $scope.wsg = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Workflow Studio GUI";
        dialog.querySelector('#dialogText').innerHTML = "Also known as the Workfow Designer, the Workflow Studio GUI is an intuitive graphical IDE tool that can be used to create or modify workflows used with Weaver.";
    }
    
    
    $scope.wefe = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Workflow Engine Front-End";
        dialog.querySelector('#dialogText').innerHTML = "<p>Responsible for creating workflow instances. Workflows are structured sequences of actions to be performed on VNF nodes. Metadata, stored in YML files, is used to map a given workflow to a specific tenant, VNF and template.</p>"+
            "<p>Weaver provides some predefined workflows, for example:</p>"+
            "<ul> <li>Instantiate, which includes updating and starting a VNF</li> <li>Start/Stop VNF</li> <li>Check whether a VNF is up/down</li> <li>Upgrade, Update, Rollback, Terminate</li> </ul>"+
            "<p>Custom workflows can also be run through the workflow front end.</p>";
    }
    $scope.we = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Workflow Engine";
        dialog.querySelector('#dialogText').innerHTML = "Enables the automation of VNF management tasks by running workflows when a request is received from the Workflow Front End. A workflow is run for each node in the system being managed by Weaver.";
    }
    $scope.rm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Resource Manager";
        dialog.querySelector('#dialogText').innerHTML = "The Resource Manager microservice exposes synchronous API interfaces to the Workflow Engine so that when a workflow is run, VNF lifecycle operations can be performed on each runtime node simultaneously.  For example, the Resource Manager exposes APIs for the following actions:"+
            "<ul> <li>Installing software</li> <li>Updating configuration</li> <li>Upgrading/Rollback software</li> <li>Start/Stop VNF components</li> <li>Scaling VNF components</li> <li>Deleting VNF components</li> </ul>";
    }
    $scope.val = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VIM Abstraction Layer";
        dialog.querySelector('#dialogText').innerHTML = "Allows mapping of infrastructure parameters from VNF-Descriptor to create a VIM specific template, for instance HEAT template for OpenStack. Other types of VIMs are also supported.";
    }
    
    
    $scope.cm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Configuration Manager";
        dialog.querySelector('#dialogText').innerHTML = "<p>The Configuration Manager microservice provides Tenant and VNF management from a VNF "+
            "repository perspective. It is used for storing and managing templates and the related information below, which is stored in an internal VNF repository at file-category level. </p>"+
            "<p>The repository includes the following types of information:</p>"+
            "<ul><li>VNF Topology</li> <li>Packages</li> <li>Procedures</li> <li>Configuration Files</li> <li>Metadata</li> <li>Workflows</li></ul><p>The Configuration Manager microservice "+
            "allows multiple VNF template developers to collaborate in development. Templates are versioned, and developers can see who created or modified a template and when it happened.</p>";
    }
    $scope.pm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Performance Manager";
        dialog.querySelector('#dialogText').innerHTML = "Collects performance information off the VNF and allows orchestration and workflows to apply specific actions, for example automated scaling.";
    }
    $scope.fm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Fault Manager";
        dialog.querySelector('#dialogText').innerHTML = "Collects alarms and logs information off the VNF and allows orchestration and workflows to apply specific actions, for example automated healing.";
    }
    $scope.iim = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VNF Instance Inventory Manager";
        dialog.querySelector('#dialogText').innerHTML = "The VNF Instance Inventory Manager (IIM) allows storing and querying of VNF instance statuses";
    }
    
    
    $scope.heat = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "HEAT";
        dialog.querySelector('#dialogText').innerHTML = "Heat provides a template based orchestration for describing a cloud application by executing appropriate OpenStack API calls to generate running cloud applications."+
                            " A Heat template describes the infrastructure for a cloud application in text files which are readable and writable by humans, and can be managed by version control tools. Weaver creates a Heat template from the VNFD at instantiation.";
    }
    $scope.ra1 = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Resource Agent / VNFC";
        dialog.querySelector('#dialogText').innerHTML = "Weaver requires the deployment of an Weaver Agent on each VNFC in the system. Each Weaver Agent exposes asynchronous API interfaces to the Resource Manager Micro-service so that they can communicate about the lifecycle operations to perform on the VNFs, on a VNFC basis, on the pre-defined procedures from the VNF template associated with the current workflow. The agent keeps track of job status, such as whether it is running, completed, or whether it failed before completing. The resulting output is returned to the Resource Manager Microservice and passed back so that the user can receive the feedback.";
    }
    $scope.ra2 = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Resource Agent / VNFC";
        dialog.querySelector('#dialogText').innerHTML = "Weaver requires the deployment of an Weaver Agent on each VNFC in the system. Each Weaver Agent exposes asynchronous API interfaces to the Resource Manager Micro-service so that they can communicate about the lifecycle operations to perform on the VNFs, on a VNFC basis, on the pre-defined procedures from the VNF template associated with the current workflow. The agent keeps track of job status, such as whether it is running, completed, or whether it failed before completing. The resulting output is returned to the Resource Manager Microservice and passed back so that the user can receive the feedback.";
    }
    
    $scope.fgui = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VNFM GUI";
        dialog.querySelector('#dialogText').innerHTML = "The Weaver VNFM GUI is an optional microservice. Users can manage the VNF Lifecycle through the VNFM GUI. It queries the Instance Inventory Manager for the state of VNFs, and it can trigger workflows in the Front End.";
    }
    
    $scope.fiim = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Instance Inventory Manager (IIM)";
        dialog.querySelector('#dialogText').innerHTML = "The Instance Inventory Manager keeps track of the status of VNFs - whether they are up/down or in an error state. The IIM responds to queries from the VNFM GUI and from the Workflow Engine.";
    }
    
    $scope.fval = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "VIM Abstraction Layer (VAL)";
        dialog.querySelector('#dialogText').innerHTML = "The VIM Abstraction Layer is the translator between the VIM, in this diagram OpenStack, and the Workflow Engine. The VAL takes requests from the Workflow Engine and translates it to a Heat template for use with OpenStack.";
    }
    
    $scope.fpm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Performance Manager";
        dialog.querySelector('#dialogText').innerHTML = "The Performance Manager monitors the performance of the VNFs via Report Metrics from the Resource Agent. It also fetches configurations from the Configuration Manager. Workflows can be triggered by the Performance Manager by scaling up/down or healing VNFs based on configured thresholds.";
    }
    
    $scope.ffe = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Front End";
        dialog.querySelector('#dialogText').innerHTML = "The Front End is a REST API endpoint that handles workflows. It takes intruction from the User via the API calls or the VNFM GUI and then instructs the Workflow Engine to trigger a workflow.";
    }
    
    $scope.fwe = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Workflow Engine";
        dialog.querySelector('#dialogText').innerHTML = "Workflow Engine is Weavers Orchestration tool for running workflows. It receives instruction from users via the Front End or from the Performance Manager or from the Fault Manager microservices. Depending on the workflow triggered, the Workflow engine then triggers action via the Resource Manager or VAL. It also fetches configuration settings from the Configuration Manager.";
    }
    
    $scope.ffm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Fault Manager";
        dialog.querySelector('#dialogText').innerHTML = "The Fault Manager receives reports from the Resource Agent and is able to trigger workflows in the Workflow Engine. It also takes configuration settings from the Configuration Manager.";
    }
    
    $scope.fra = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Resource Agent";
        dialog.querySelector('#dialogText').innerHTML = "The Resource Agent is a microservice that is deployed on each VNFC to install components on the VNFC, report on faults, report metrics to the Performance Manager and take instruction from the Resource Manager";
    }
    
    $scope.fcm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Configuration Manager";
        dialog.querySelector('#dialogText').innerHTML = "The Configuration Manager is a centralised point for configuration settings for the Resource Agent, the Performance manager, the Fault Manager and the Workflow Engine. Metadata files and Jinja files are used for configuration. ??";
    }
    
    $scope.frm = function() {
        dialog.showModal();
        dialog.querySelector('.mdl-dialog__title').innerHTML = "Resource Manager";
        dialog.querySelector('#dialogText').innerHTML = "The Resource Manager takes instruction from the Workflow Engine and communicates it to the various Resource Agents asynchronously.";
    }

}]);
