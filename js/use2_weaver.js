app.controller('use2Controller', ['$scope', '$rootScope', '$mdDialog', '$q', '$http', '$timeout', 'getRequest', 'postRequest', 'createSubscriber', 'querySubscriber', 'displayRequestDialog', 'addOffer', 'displayToast', 'displayRequestDialog', 'logAction', 'readTextFile', 'showCreateDialog', 'logNavigation', function($scope, $rootScope, $mdDialog, $q, $http, $timeout, getRequest, postRequest, createSubscriber, querySubscriber, displayRequestDialog, addOffer, displayToast, displayRequestDialog, logAction, readTextFile, showCreateDialog, logNavigation){
    $rootScope.pageUrl = url;
    $scope.appSelectorPort = appSelectorPort;
    $scope.useCaseTitle = "Use Case 2";
    $scope.useCaseSubTitle = "Lifecycle Management of a third party VNF";

    
	$scope.checkRegistration = function(){
        readTextFile("js/Registration.json", function(data){
            data = JSON.parse(data);
            console.log(data);

            if (data.user.firstName == ""){
                showCreateDialog();
            } else {
                $rootScope.username = data.user.firstName + " " + data.user.secondName;
                console.log("Username : " + $rootScope.username);
            }
        });
    }
    $scope.checkRegistration();

    $scope.checkVersion = function(){
        readTextFile("js/version.json", function(data){
            data = JSON.parse(data);
            console.log(data);


            if (data.version.number != ""){
                $rootScope.version = data.version.number;
            }
        });
    }
    $scope.checkVersion();

    $scope.logNavigation = logNavigation;
    
    
    
    
    $scope.toStep1 = function() {
        $timeout(function() {
            navigateToPage("#step-2");
        }, 100);
    }

    $scope.toStep2 = function() {
        $timeout(function() {
            navigateToPage("#step-3");
        }, 100);
    }

    $scope.toStep3 = function() {
        $timeout(function() {
            navigateToPage("#step-4");
        }, 100);
    }

    $scope.toStep4 = function() {
        $timeout(function() {
            navigateToPage("#step-5");
        }, 100);
    }
    
    $scope.toStep5 = function() {
        $timeout(function() {
            navigateToPage("#step-6");
        }, 100);
    }
    
    $scope.toStep6 = function() {
        $timeout(function() {
            navigateToPage("#step-7");
        }, 100);
    }
    
    $scope.toStep7 = function() {
        $timeout(function() {
            navigateToPage("#step-8");
        }, 100);
    }
    
    $scope.toStep8 = function() {
        $timeout(function() {
            navigateToPage("#step-9");
        }, 100);
    }
    
    $scope.toStep9 = function() {
        $timeout(function() {
            navigateToPage("#step-10");
        }, 100);
    }
    
    $scope.toStep10 = function() {
        $timeout(function() {
            navigateToPage("#step-11");
        }, 100);
    }
    
    $scope.toStep11 = function() {
        $timeout(function() {
            navigateToPage("#step-12");
        }, 100);
    }

    var querySubscribersQueue = [];

    // Called at the start of a use case
    


}]);
